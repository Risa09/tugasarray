package org.example;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ArrayList<String> kucing = new ArrayList<>();
        kucing.add("mochi");
        kucing.add("bona");
        kucing.add("lupus");
        kucing.add("puchi");
        kucing.add("millo");

        System.out.println("============= Kucing ============= ");

        for (String pet : kucing) {
            System.out.println(pet);
        }


        ArrayList<String> meow = new ArrayList<>();
        meow.add("millo");
        meow.add("pipip");
        meow.add("grizzy");
        meow.add("lupus");


        System.out.println("\n============= Meow ============= ");

        for (String pet : meow ) {
            System.out.println(pet);
        }



        ArrayList<String> Nama_kucing = new ArrayList<String>(kucing);

        Nama_kucing.retainAll(meow);
        kucing.addAll(meow);
        kucing.removeAll(Nama_kucing);


        System.out.println("====Mypets=====");

        Iterator itr = kucing.iterator();

        while (itr.hasNext()){

            System.out.println(itr.next());
        }





    }
}
